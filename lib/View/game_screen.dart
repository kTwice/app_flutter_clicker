import 'dart:async';
import 'package:flutter_application_2_clicker/Model/games_manager.dart';
import 'package:flutter_application_2_clicker/generated/l10n.dart';

import 'hall_of_fame.dart';

import 'package:flutter/material.dart';

class GameScreen extends StatelessWidget {
  final gamesManager = GamesManager();

  GameScreen(BuildContext context, {super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text(
          "Kev  Clicker",
          textAlign: TextAlign.center,
        ),
        leading: IconButton(
          icon: const Icon(Icons.menu),
          onPressed: () {},
        ),
      ),
      body: SafeArea(
        child: FutureBuilder(
          future: gamesManager.loadGamesListFromLocalData(),
          builder: (context, snapshot) {
            if (snapshot.hasData || snapshot.hasError) {
              return _GamesScreenContent(
                gamesManager: gamesManager,
              );
            } else {
              return const CircularProgressIndicator();
            }
          },
        ),
      ),
    );
  }
}

class _GamesScreenContent extends StatefulWidget {
  final GamesManager gamesManager;
  const _GamesScreenContent({Key? key, required this.gamesManager})
      : super(key: key);

  @override
  State<_GamesScreenContent> createState() =>
      __GamesScreenContentState(gamesManager);
}

class __GamesScreenContentState extends State<_GamesScreenContent> {
  final GamesManager gamesManager;
  String _pseudo = "";
  final _formKey = GlobalKey<FormState>();

  __GamesScreenContentState(this.gamesManager);

  void _startCounting() {
    setState(() {
      gamesManager.startNewGame(userName: _pseudo);
      Timer(const Duration(seconds: GamesManager.gamesDuration), _stopGame);
    });
  }

  void _clickedButtonTouched() {
    setState(() {
      gamesManager.currentGame?.scoring();
    });
  }

  void _stopGame() {
    setState(() {
      gamesManager.finishCurrentGame();
    });
  }

  void _changeName(value) {
    setState(() {
      _pseudo = value;
    });
  }

  void _validateNewName() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
    }
  }

  @override
  Widget build(BuildContext context) {
    final bestGame = gamesManager.bestGame;
    final currentGame = gamesManager.currentGame;

    return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            S.current.hello(_pseudo),
            style: Theme.of(context).textTheme.displaySmall,
          ),
        ],
      ),
      //if (!gamesManager.isGameInProgress)
      Form(
        key: _formKey,
        child: Row(
          children: [
            const Icon(Icons.person),
            Expanded(
              child: TextFormField(
                decoration: const InputDecoration(
                    helperText: "Entrez votre prénom", hintText: "Prénom"),
                autocorrect: false,
                textCapitalization: TextCapitalization.words,
                autofillHints: const [AutofillHints.givenName],
                keyboardType: TextInputType.name,
                validator: (value) =>
                    value!.length > 2 ? null : "prénom trop court",
                onSaved: _changeName,
                //onSubmitted: _changeName,
              ),
            ),
            IconButton(
                onPressed: _validateNewName, icon: const Icon(Icons.check)),
          ],
        ),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (bestGame != null)
            Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(S.of(context).record_Best_score_Point(
                    bestGame.playerName, bestGame.score))),
        ],
      ),
      if (currentGame != null)
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              S.of(context).score_point(currentGame.score),
            ),
          ],
        ),
      if (currentGame != null)
        IconButton(
            onPressed: _clickedButtonTouched, icon: const Icon(Icons.plus_one)),
      //const Image(image: AssetImage('images/gandalf.png')),
      const Spacer(),
      if (!gamesManager.isGameInProgress && _pseudo != "")
        ElevatedButton(
            onPressed: _startCounting, child: const Text("Démarrer la partie")),
      ElevatedButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) =>
                  HallOfFame(resultList: gamesManager.bestGameList)));
        },
        child: const Text("Afficher le hall of fame"),
      ),
    ]);
  }
}
