// ignore_for_file: must_be_immutable

import 'package:flutter_application_2_clicker/Model/game.dart';

import 'package:flutter/material.dart';

class HallOfFame extends StatelessWidget {
  final List<Game> _resultList;

  const HallOfFame({super.key, required resultList}) : _resultList = resultList;

  Widget _makeRow(BuildContext context, int rowNumber) {
    final result = _resultList[rowNumber];
    return ListTile(
        title: Text(result.playerName),
        subtitle: Text("${result.score} points"),
        leading: const Icon(Icons.people_alt),
        trailing: const Icon(Icons.score));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Scores")),
      body: SafeArea(
        child: ListView.builder(
            itemCount: _resultList.length, itemBuilder: _makeRow),
      ),
    );
  }
}
