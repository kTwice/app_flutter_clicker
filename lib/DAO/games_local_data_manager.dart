import 'dart:async';

import 'package:floor/floor.dart';
import 'package:flutter_application_2_clicker/Model/game.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

part 'games_local_data_manager.g.dart';

class GamesLocalDataManager {
  Future<GameDao> get _gameDao async {
    final database =
        await $FloorAppDatabase.databaseBuilder('app_database.db').build();
    return database.gameDao;
  }

  Future<void> addNewGame(Game game) async {
    final gameDao = await _gameDao;
    return gameDao.insertGame(DbGame.fromGame(game));
  }

  Future<List<Game>> getGamesList() async {
    final gameDao = await _gameDao;
    final dbGamesList = await gameDao.findAllGames();
    return dbGamesList.map((dbGame) => dbGame.game).toList();
  }
}

@dao
abstract class GameDao {
  @Query('SELECT * FROM DbGame')
  Future<List<DbGame>> findAllGames();

  @insert
  Future<void> insertGame(DbGame games);
}

@Entity()
class DbGame {
  @PrimaryKey(autoGenerate: true)
  int? id;

  final String playerName;

  final int score;

  DbGame(this.id, this.playerName, this.score);

  DbGame.fromGame(Game game)
      : this.playerName = game.playerName,
        this.score = game.score;

  Game get game => Game(playerName: playerName, score: score);
}

@Database(version: 1, entities: [DbGame])
abstract class AppDatabase extends FloorDatabase {
  GameDao get gameDao;
}
