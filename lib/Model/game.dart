class Game implements Comparable<Game> {
  final String playerName;
  int _score;
  bool _isInProgress = false;

  Game({required this.playerName, score = 0}) : _score = score;

  int get score => _score;
  bool get isInProgress => _isInProgress;

  start() {
    _score = 0;
    _isInProgress = true;
  }

  finish() {
    _isInProgress = false;
  }

  scoring() {
    if (_isInProgress) {
      _score++;
    }
  }

  @override
  int compareTo(Game other) {
    return this.score.compareTo(other.score);
  }
}
