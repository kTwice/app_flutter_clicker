import 'package:flutter/material.dart';
import 'package:flutter_application_2_clicker/View/game_screen.dart';
import 'package:flutter_application_2_clicker/generated/l10n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  ThemeData _buildCustomTheme() {
    final base = ThemeData.light();
    final colorScheme =
        ColorScheme.fromSeed(seedColor: Color.fromARGB(255, 45, 32, 228));
    return base.copyWith(colorScheme: colorScheme);
  }

  ThemeData _buildCustomDarkTheme() {
    final base = ThemeData.dark();
    final colorScheme =
        ColorScheme.fromSeed(seedColor: Color.fromARGB(255, 197, 7, 235));
    return base.copyWith(colorScheme: colorScheme);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'kev Clicker',
      theme: _buildCustomTheme(),
      darkTheme: _buildCustomDarkTheme(),
      home: GameScreen(context),
      localizationsDelegates: const [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: S.delegate.supportedLocales,
    );
  }
}
